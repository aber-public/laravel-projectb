<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiOrderController;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');

Route::get('orders', [ApiOrderController::class, 'index']);
Route::get('orders/{order}', [ApiOrderController::class, 'show']);
Route::post('orders', [ApiOrderController::class, 'store']);
Route::put('orders/{order}', [ApiOrderController::class, 'update']);
Route::delete('orders/{order}', [ApiOrderController::class, 'delete']);
Route::put('orders/{order}/add', [ApiOrderController::class, 'addProduct']);
Route::put('orders/{order}/pay', [ApiOrderController::class, 'payOrder']);

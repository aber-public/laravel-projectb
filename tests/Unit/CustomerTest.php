<?php
use App\Models\Customer;

test('customer', function () {
    $customer = new Customer([
        'first_name' => 'FirstName',
        'last_name' => 'LastName',
        'email' => 'FirstName.LastName@company.x'
    ]);
    $this->assertInstanceOf(Customer::class, $customer);
    $this->assertEquals('FirstName', $customer->first_name);
    $this->assertEquals('LastName', $customer->last_name);
    $this->assertEquals('FirstName.LastName@company.x', $customer->email);
});

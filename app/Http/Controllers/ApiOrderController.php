<?php

namespace App\Http\Controllers;

use App\Services\PaymentService;
use Illuminate\Http\Request;
use Log;

use App\Models\Order;

class ApiOrderController extends Controller
{
    public function index()
    {
        return Order::with('Products')->get();
    }

    public function show(Order $order)
    {
        return $order;
    }

    public function store(Request $request)
    {
        Log::debug("Save request: " . implode(", ", $request->all()));

        $order = Order::create($request->all());

        return response()->json($order, 201);
    }

    public function update(Request $request, Order $order)
    {
        $order->update($request->all());

        return response()->json($order, 200);
    }

    public function delete(Order $order)
    {
        $order->delete();

        return response()->json(null, 204);
    }

    public function addProduct(Request $request, Order $order)
    {
        Log::debug("Add product " . $request['product_id'] . " to order " . $order->id);

        if (!$order->paid) {
            $order->products()->attach($request['product_id']);

            return response()->json($order, 200);
        } else {
            return response()->json($order, 400);
        }
    }

    public function payOrder(Request $request, Order $order, PaymentService $paymentService)
    {
        Log::debug("Pay order " . $order->id);
        
        $response = $paymentService->pay($order);

        if ($response == "OK") {
            return response()->json(["message" => $response], 200);
        } else {
            return response()->json([
                "order" => $order,
                "message" => $response
            ], 400);
        }
    }
}

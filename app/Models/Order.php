<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Orders => (ID,customer,payed,created_at)
 */
class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'customer',
        'payed',
        'created_at',
    ];

    public function products() {

        return $this->belongsToMany(
            Product::class,
            'orders_products',
            'order_id',
            'product_id');
    }
}

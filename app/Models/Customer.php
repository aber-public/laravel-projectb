<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Customer: (ID,Job Title,Email Address,FirstName LastName,registered_since,phone)
 */
class Customer extends Model
{
    use HasFactory;

    protected $fillable = [
        'job_title',
        'email',
        'first_name',
        'last_name',
        'registered_since',
        'phone'
    ];
}

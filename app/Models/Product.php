<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Products: (ID,productname,price)
 */
class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'price'
    ];

    public function orders()
    {
        return $this->belongsToMany(
            Order::class,
            'orders_products',
            'order_id',
            'pro_id');
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Log;

use App\Traits\DataImportTrait;

class CustomerImport extends Command
{
    use DataImportTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:customer-import {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to import customer data';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        Log::info('Command import CUSTOMER data...');

        $headersMappingFct = function ($header) {
                switch ($header) {
                    case 'FirstName LastName':
                        return 'name';
                    case 'Job Title':
                        return 'job_title';
                    case 'Email Address':
                        return 'email';
                    default:
                        return $header;
                    }
        };

        $count = $this->readCsvWithHeaders($this->argument('file'), 'customers', $headersMappingFct);
        
        $this->info('Customers imported successfully. ' . $count);
        return 0;
    }
}

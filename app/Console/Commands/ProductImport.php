<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Log;

use App\Traits\DataImportTrait;

class ProductImport extends Command
{
    use DataImportTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:product-import {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to import product data';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        Log::info('Command import product data...');

        $count = $this->readCsvWithHeaders($this->argument('file'), 'products');
        
        $this->info('products imported successfully. ' . $count);
        return 0;
    }
}

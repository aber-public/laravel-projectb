<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;
use Log;

class SuperPaymentService
{
    private string $url = "https://superpay.view.agentur-loop.com";

    /**
     * Create a new class instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * API payment service method implementation for "https://superpay.view.agentur-loop.com/pay"
     * (with retries)
     */
    public function pay(array $data, int $retry = 0) {
        // Note: considered https://laravel.com/docs/11.x/http-client#retries 
        // but custom error handling seems to be favorable
        $response = Http::withBody(json_encode($data), 'application/json')
            ->post($this->url . '/pay');

        if ($response->status() != 200 && $retry < 3) {
            sleep(1);
            $retry++;
            return $this->pay($data, $retry);

        } else if ($response->status() == 200) {
            $message = json_decode($response->body())->message;
            if ($message == "Payment Successful") {
                return "OK";
            }
            Log:info("Payment message:" . $message);
            return $message;
        } else {
            Log::info($response);
        }

        return ["API-response" => $response];
    }
}

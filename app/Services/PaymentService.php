<?php

namespace App\Services;

use Log;

use App\Models\Order;
use App\Models\Customer;

class PaymentService
{
    private $superPaymentService;

    /**
     * Create a new class instance.
     */
    public function __construct(SuperPaymentService $superPaymentService) 
    {
        $this->superPaymentService = $superPaymentService;
    }

    //Note: Order should get locked at this point (race condition add product before paid)
    public function pay (Order $order) {
        $orderWithProducts = Order::with('Products')->find($order->id);
        Log::debug("Order with products to pay:\n" . $orderWithProducts);

        if ($orderWithProducts -> payed){
            return "Already payed";
        } else {
            $customer = Customer::find($order->customer);

            $orderSum = 0.0;
            foreach ($orderWithProducts->products as $product) {
                $orderSum += $product->price;
            };
            $orderSum = ceil($orderSum * 100) / 100;

            $paymentRequest = [
                "order_id" => $order->id,
                "customer_email" => $customer->email,
                "value" => $orderSum
            ];

            Log::debug($paymentRequest);
            
            $result = $this->superPaymentService->pay($paymentRequest);

            if ($result == "OK") {
                Log::debug("Payment successful, updating order!");
                $orderWithProducts->payed = true;
                $orderWithProducts->save();
            }

            return $result;
        }
    }
}

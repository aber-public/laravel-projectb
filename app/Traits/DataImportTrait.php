<?php
namespace App\Traits;

use Illuminate\Support\Facades\DB;

// https://blog.devgenius.io/laravel-import-csv-data-into-table-via-console-command-b800e8d5e18c
trait DataImportTrait {
    /**
     *         $headersMappingFct = function ($header) {
     *               switch ($header) {
     *                   case 'FirstName LastName':
     *                       return 'name';
     *                   case 'Job Title':
     *                       return 'job_title';
     *                   case 'Email Address':
     *                       return 'email';
     *                   default:
     *                       return $header;
     *                   }
     *       };
     *
     *       $this->readCsvWithHeaders($file, 'customers', $headersMappingFct);
     */
    public function readCsvWithHeaders(string $file, string $tablename, $headersMapping = null): int
    {   
        if (!file_exists($file)) {
            $this->error('The specified file does not exist.');
            return 1;
        }
        
        $data = array_map('str_getcsv', file($file));
        $headers = array_shift($data);

        if (isset($headersMapping)) {
            $headers = array_map($headersMapping, $headers);
        }

        $allRecords = $this->combineHeadersAndData($headers, $data);

        // Split the array into batches of 5
        $batchOf = 500;
        $batch = array_chunk($allRecords, $batchOf);
        $count = 0;

        // Process each batch
        foreach ($batch as $chunk) {
            DB::table($tablename)->insert($chunk);
            $count += count($chunk);
        }

        return $count;
    }

    /**
     * Combine headers and data into associative arrays.
     *
     * @param array $headers
     * @param array $data
     * @return array
     */
    private function combineHeadersAndData(array $headers, array $data)
    {
        $combinedData = [];
        
        foreach ($data as $row) {
            $combinedData[] = array_combine($headers, $row);
        }
        
        return $combinedData;
    }
}